﻿using KnowledgeTestingSystemWebApi.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KnowledgeTestingSystemWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {
        private readonly TestingSystemContext _context;


        public TestController(TestingSystemContext context)
        {
            _context = context;
        }

        // GET: api/Test
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Test>>> GetTests()
        {
            return await _context.DbTests.ToListAsync();
        }

        // GET: api/Test/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Test>> GetTest(int id)
        {
            var test = await _context.DbTests.FindAsync(id);

            if (test == null)
            {
                return NotFound();
            }

            return test;
        }

        [HttpGet("Test/{userId}")]
        public ActionResult<IEnumerable<Test>> GetTestsByUser(int userId)
        {
            List<Test> testContext = null; 
            foreach (var test in _context.DbTests)
            {
                if (test.CreatorId == userId)
                {
                    testContext.Add(test);
                }
            }
            return testContext;
        }

        [HttpPost]
        public async Task<ActionResult<Test>> PostTest(Test test)
        {
            _context.DbTests.Add(test);
            await _context.SaveChangesAsync();

            return CreatedAtAction("PostTest", new { id = test.Id }, test);
        }



    }
}
