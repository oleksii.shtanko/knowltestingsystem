﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace KnowledgeTestingSystemWebApi.Models
{
    public class TestHistory
    {

        [Key]
        public int Id { get; set; }
        [Column(TypeName = "int")]
        public int UserId { get; set; }
        [Column(TypeName = "int")]
        public int Score { get; set; }
        [Column(TypeName = "nvarchar(Max)")]
        public string TimeFinish { get; set; }
        [Column(TypeName = "nvarchar(100)")]
        public int Status { get; set; }
    }
}
